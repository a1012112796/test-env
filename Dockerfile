FROM golang:1.17-bullseye
ARG GITEA_ID=1000
ARG GITEA_GID=1000

RUN addgroup \
    --gid $GITEA_GID \
    gitea && \
  adduser \
    --gecos '' \
    --shell /bin/bash \
    --uid $GITEA_ID \
    --gid $GITEA_GID \
    gitea

# upgrade git to v2.35.0
RUN curl -SL https://github.com/git/git/archive/v2.35.0.tar.gz \
    | tar -xzv -C /go \
    && apt-get update \
    && apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev \
    && make -C /go/git-2.35.0 prefix=/usr all \
    && make -C /go/git-2.35.0 prefix=/usr install \
    && rm -rf /go/git-2.35.0 \
# install git-lfs
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y git-lfs \
# install golangci-lint
    && export BINARY="golangci-lint" \
    && curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/v1.43.0/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.44.0
